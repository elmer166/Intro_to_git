<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % BOOK_ENTITIES SYSTEM "Intro_to_git.ent">
%BOOK_ENTITIES;
]>
<section id="how">
  <title>So how does this work</title>
  <para>
    Central to a <application>git</application> project is the
    <emphasis>working directory</emphasis>.  The working directory,
    surprise, surprise, is the directory where you do your work.  The
    working directory includes all the subdirectories,
    sub-subdirectories, etc. in your project.
  </para>
  <para>
    <figure float="0">
      <title>Working tree</title>
      <mediaobject>
        <imageobject><imagedata scale="50" scalefit="1" 
        fileref="images/Directories.png" format="PNG"/>
        </imageobject>
        <textobject>
          <para>
            git working tree diagram
          </para>
        </textobject>
      </mediaobject>
    </figure>
  </para>
  <para>
    Within the working directory is a subdirectory named
    <filename>.git</filename>. This is where
    <application>git</application> keeps its record of your project.
    The current content and entire history of all the files in your
    project are maintained here.  This is called the <emphasis>local
    repository</emphasis> and almost all the capability of
    <application>git</application> is implemented in this directory.
  </para>
  <para>
    The contents of this folder are pretty
    cryptic. <application>git</application> keeps
    <emphasis>SHA1</emphasis> digests of everything in this
    directory.  In fact, pretty much everything about
    <application>git</application> is maintained in SHA1 digests.
    This is quite an efficient way to do it.
    <application>git</application> only keeps a copy of a disk block
    if it is different.  Any disk blocks that are the same, even if
    they are parts of different files, are only stored once.
  </para>
  <para>
    The repository may optionally reference one or more
    <emphasis>remotes</emphasis>. A remote is a copy of your
    <filename>.git</filename> directory, conceptually on some other
    computer.  However, it is quite possible to have a remote on the
    same computer.  The remote provides a way to collaborate with
    others.  It also can make a pretty handy backup.
  </para>
  <para>
    There are a number of public sites that provide a place to put a
    remote.  The most common are GitLab and GitHub, but there are
    quite a few others.  Most of these sites offer some additional
    features such as fine grained control over who can access your
    project, and things like graphical reports and a wiki where
    multiple developers can record conversations about the project.
  </para>
  <para>
    A remote has a name and a location.  The location is a URI,
    usually of the form <filename>user@site://</filename>,
    <filename>git://</filename>, or <filename>file://</filename>.  The
    name is anything you like.  If you do not provide a name,
    <application>git</application> calls it
    <filename>origin</filename>.  Obviously, you can have only one
    <filename>origin</filename>, but you may have many remotes of
    other names.
  </para>
</section>
